/*****************************************************************************

  Licensed to Accellera Systems Initiative Inc. (Accellera) under one or
  more contributor license agreements.  See the NOTICE file distributed
  with this work for additional information regarding copyright ownership.
  Accellera licenses this file to you under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with the
  License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
  implied.  See the License for the specific language governing
  permissions and limitations under the License.

 *****************************************************************************/

/*****************************************************************************

  sc_mempool.h - Memory pools for small objects.

  Original Author: Stan Y. Liao, Synopsys, Inc.

  CHANGE LOG AT END OF FILE
 *****************************************************************************/

#ifndef SC_MEMPOOL_H
#define SC_MEMPOOL_H

//HamletG begins
#include <vector>

#if defined(_MSC_VER) && !defined(SC_WIN_DLL_WARN)
#pragma warning(push)
#pragma warning(disable: 4251) // DLL import for std::vector
#endif

namespace sc_core
{

class SC_API sc_mempool_client_handle;

class SC_API sc_mempool_client
{
public:
    friend class SC_API sc_mempool_client_handle;

    sc_mempool_client();
    virtual ~sc_mempool_client();

    void SetHandle(sc_mempool_client_handle *handle);
    sc_mempool_client_handle *GetHandle();

    void CleanUp();
    void CleanedUp();
    bool GetCleanedUp();

    virtual void pool_clean_up()=0;
protected:
    void SetCleanedUp(bool cleaned_up);

    sc_mempool_client_handle *m_handle = nullptr;
    bool m_cleaned_up = false;
};

class SC_API sc_mempool_client_handle
{
public:
    sc_mempool_client_handle();
    virtual ~sc_mempool_client_handle();

    void SetClient(sc_mempool_client *client);
    sc_mempool_client *GetClient();

    void SetCleanedUp(bool cleaned_up);
    bool GetCleanedUp();
protected:
    sc_mempool_client *m_client;
    bool m_cleaned_up;
};

//HamletG ends
// ----------------------------------------------------------------------------
//  CLASS : sc_mempool
//
//  ...
// ----------------------------------------------------------------------------

class SC_API sc_mempool
{
public:
    friend class SC_API sc_mempool_client;

    static void* allocate( std::size_t sz );
    static void release( void* p, std::size_t sz );
    static void display_statistics();

    static bool uses_default_new();  //HamletG
    static bool cleaned_up();

    static void register_client(sc_mempool_client *client);

    static void clean_up();                 //HamletG
protected:
    static bool m_cleaned_up;
    static std::vector<sc_mempool_client_handle *> *m_clients;
};


// ----------------------------------------------------------------------------
//  CLASS : sc_mpobject
//
//  ...
// ----------------------------------------------------------------------------

class SC_API sc_mpobject
{
public:

    static void* operator new( std::size_t sz )
    {
        return sc_mempool::allocate( sz );
    }

    static void operator delete( void* p, std::size_t sz )
    {
        sc_mempool::release( p, sz );
    }

    static void* operator new[]( std::size_t sz )
    {
        return sc_mempool::allocate( sz );
    }

    static void operator delete[]( void* p, std::size_t sz )
    {
        sc_mempool::release( p, sz );
    }
};

} // namespace sc_core

#if defined(_MSC_VER) && !defined(SC_WIN_DLL_WARN)
#pragma warning(pop)
#endif

// $Log: sc_mempool.h,v $
// Revision 1.3  2011/08/26 20:46:18  acg
//  Andy Goodrich: moved the modification log to the end of the file to
//  eliminate source line number skew when check-ins are done.
//
// Revision 1.2  2011/02/18 20:38:44  acg
//  Andy Goodrich: Updated Copyright notice.
//
// Revision 1.1.1.1  2006/12/15 20:20:06  acg
// SystemC 2.3
//
// Revision 1.3  2006/01/13 18:53:11  acg
// Andy Goodrich: Added $Log command so that CVS comments are reproduced in
// the source.

#endif
