@echo off

if defined ESYSSDK (
    echo Found ESysSDK at %ESYSSDK%
) else (
    echo Warning : ESysSDK not found
)

set MYSYSC_DEV=%~dp0\..\..
set SYSTEMC_HOME=%~dp0\..\..
set LOGMOD=%~dp0\..\..\extlib\dbg_log

echo. 

if defined ESYSSDK ( 
    echo Use call_msvc.exe to start Visual C++
    "%ESYSSDK%\bin\call_msvc.exe" msvc14 "%~dp0\SystemC.sln"
) else (    
    echo Use default location where Visual C++ is installed to start it
    "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.exe" %~dp0\SystemC.sln
)



